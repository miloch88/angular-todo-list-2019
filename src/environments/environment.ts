// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  config: {
    apiKey: 'AIzaSyAyQ2MHj-ENK7WE_2Oozl2NH4a0cKzHee4',
    authDomain: 'todo-test-63b24.firebaseapp.com',
    databaseURL: 'https://todo-test-63b24.firebaseio.com',
    projectId: 'todo-test-63b24',
    storageBucket: 'todo-test-63b24.appspot.com',
    messagingSenderId: '530490655570'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
