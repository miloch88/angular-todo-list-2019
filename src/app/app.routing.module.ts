import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TodoTaskComponent} from './todo-task/todo-task.component';
import {DoneTaskComponent} from './done-task/done-task.component';
import {LoginComponent} from './auth/login/login.component';
import {AuthQuardService} from './auth/auth-quard.service';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/todoTask',
    pathMatch: 'full'
  },
  {
    path: 'todoTask',
    component: TodoTaskComponent,
    canActivate: [AuthQuardService]
  },
  {
    path: 'doneTask',
    component: DoneTaskComponent,
    canActivate: [AuthQuardService]
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
