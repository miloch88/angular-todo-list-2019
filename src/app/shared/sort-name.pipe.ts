import { Pipe, PipeTransform } from '@angular/core';
import {Task} from '../models/task';
import {el} from '@angular/platform-browser/testing/src/browser_util';

@Pipe({
  name: 'sortName',
})

export class SortNamePipe implements PipeTransform {

  transform(value: Array<Task>, args?: any): any {
    return value.sort((a, b) => {
      if (a.name.toLowerCase() > b.name.toLocaleLowerCase()){
        return 1;
      } else {
        return -1;
      }
    });
  }

}
