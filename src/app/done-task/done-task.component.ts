import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TasksService} from '../services/tasks.service';
import {Task} from '../models/task';

@Component({
  selector: 'app-done-task',
  templateUrl: './done-task.component.html',
  styleUrls: ['./done-task.component.css']
})
export class DoneTaskComponent implements OnInit {


  tasksDone = [];


  constructor(private tasksTaskService: TasksService) {
    this.tasksTaskService.getTasksListObs().subscribe((tasks: Array<Task>) => {
      this.tasksDone = tasks.filter(t =>  t.isDone === true);
    });
  }

  ngOnInit() {
  }



}
