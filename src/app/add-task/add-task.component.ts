import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TasksService} from '../services/tasks.service';
import {Task} from '../models/task';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {

  addForm: FormGroup;

  constructor(private tasksTaskService: TasksService,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.addForm = this.initForm();
  }

  initForm() {
    return new FormGroup({
      taskName: new FormArray([new FormControl(null, Validators.required)])
    });
  }


  add() {
    const taskList = this.createTaskList();
    this.tasksTaskService.add(taskList);
    this.addForm = this.initForm();
  }

  createTaskList(): Array<Task> {
    const taskList = new Array<Task>();
    const tasksArr = <[string]>this.addForm.get('taskName').value;
    tasksArr.forEach(taskName => {
      const task = {name: taskName, userId: this.authService.user.uid, created: new Date().toLocaleString(), isDone: false};
      taskList.push(task);
    });
    return taskList;
  }

  addField() {
    const arr = <FormArray>this.addForm.get('taskName');
    arr.push(new FormControl(null, Validators.required));
  }

}
